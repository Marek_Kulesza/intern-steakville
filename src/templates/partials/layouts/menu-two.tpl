<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <section class="menu-two">
        <div class="l-inner">
            <div class="wrapper">
                <h2><span>Steakville</span>OUR MENU</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                <div class="l-menu">
                    <ul class="menu-list">
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Poblano Queso Spinach Dip<span class="wild">Creamy queso is infused with spicy Poblano peppers</span></p>
                                <p>$14.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Boneless Buffalo Wings<span class="wild">These wings are legendary!</span></p>
                                <p>$52.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Grilled Filet Mignon<span class="wild">The most tender of all steaks!</span></p>
                                <p>$32.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Poblano Queso Spinach Dip<span class="wild">Creamy queso is infused with spicy Poblano peppers</span></p>
                                <p>$14.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Grilled Cedar Plank Salmon <span class="wild">A fresh filet of North Atlantic salmon is rubbed...</span></p>
                                <p>$14.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Grilled Cedar Plank Salmon <span class="wild">A fresh filet of North Atlantic salmon is rubbed...</span></p>
                                <p>$14.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Mozzarella Moons<span class="wild">Mozzarella cheese</span></p>
                                <p>$42.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Grilled Filet Mignon<span class="wild">The most tender of all steaks!</span></p>
                                <p>$32.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Boneless Buffalo Wings<span class="wild">These wings are legendary!</span></p>
                                <p>$52.99</p>
                            </div>
                        </li>
                        <li>
                            <div class="menu-list__item">
                                <img alt="" class="img-menu" src="">
                                <p>Mozzarella Moons<span class="wild">Mozzarella cheese</span></p>
                                <p>$42.99</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</body>
</html>