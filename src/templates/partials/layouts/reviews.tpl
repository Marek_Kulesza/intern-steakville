<section>
    <div class="l-hero">
        <div class="l-inner">
            <div class="left-side">
                    <span class="arrow"></span>
            </div>
            <div class="subscription-box">
                    <span class="center-icon"></span></div>
                    <p class="text__details"></p>
                    <p class="info">JOHN WICK<br>
                        <span class="font-wild"></span>
                    </p>
                    <ul class="c-pagination">
                        <li><span class="square"></span></li>
                        <li><span class="square"></span></li>
                        <li><span class="square"></span></li>
                    </ul>
            </div>
            <div class="right-side">
            </div>
        </div>
    </div>
</section>