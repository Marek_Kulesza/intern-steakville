<section class="menu">
    <div class="hero">
        <div class="title-box">
            <h2 class="title"><span class="wild">Our Dishes</span>MENU</h2>
        </div>
        <div class="burgers">
            <div class="burger-left">
                <img src="" alt="">
                <div class="burger-content">
                    <h2 class="burger-title">BBQ CHICKEN <span class="wild">Big and Meaty</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    <p>$50.00</p>
                </div>
            </div>
            <div class="burger-right">
                <img src="" alt="">
                <div class="burger-content">
                    <h2 class="burger-title">WHOPE SANDWICH <span class="wild">Big and Meaty</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    <p>$45.50</p>
                </div>
            </div>
            <div class="burger-left">
                <img src="" alt="">
                <div class="burger-content">
                    <h2 class="burger-title">HOMESTYLE BURGER <span class="wild">Big and Meaty</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    <p>$34.00</p>
                </div>
            </div>
            <div class="burger-right">
                <img src="" alt="">
                <div class="burger-content">
                    <h2 class="burger-title">BACON SANDWICH <span class="wild">Big and Meaty</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    <p>$75.00</p>
                </div>
            </div>
        </div>
    </div>
</section>