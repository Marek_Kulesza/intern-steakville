<section class="our-chefs">
    <div class="hero">
            <div class="title">
            <h2><span class="wild">Your Welcome</span>OPENING HOURS</h2>
            <div class="wrapper">
                <div class="img-hours">
                    <img src="" alt="">
                </div>
                <div class="box-hours">
                    <p class="bold">DINNER:</p>
                    <p>Tuesday to Saturday 5.30 pm til 9.00 pm<br>
                        Bank Holiday Sundays 6pm til 9pm</p>
                    <p class="bold">LUNCH:</p>
                    <p>Friday 12.30 til 2.30 pm<br>
                        Sunday 12.30 til 4 pm</p>
                </div>
            </div>
        </div>
    </div>
    <div class="hero">
        <div class="title-box">
            <h2><span class="wild">Our Staff</span>MEET OUR CHEFS</h2>
        </div>
        <div class="staff">
            <ul class="staff-list">
                <li>
                    <img src="" alt="" class="round-img">
                    <p>JAMES T KIRK<span class="wild">Captain</span></p>
                    <ul class="icons">
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore </p>
                </li>
                <li>
                    <img src="" alt="" class="round-img">
                    <p>LEONARD MCCOY<span class="wild">Doctor Not A...</span></p>
                    <ul class="icons">
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                </li>
                <li>
                    <img src="" alt="" class="round-img">
                    <p>HIKARU SULU<span class="wild">Pilot</span></p>
                    <ul class="icons">
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                        <li><a href=""><span></span></a></li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                </li>
            </ul>
        </div>
    </div>
</section>