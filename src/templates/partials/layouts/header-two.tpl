<header class="l-header-two">
    <div class="logo-two"><img src="" alt="logo" class="logo-two_img"></div>
    <nav class="l-navbar-two">
        <ul class="c-static-links-list-header-two">
            <li>
                <a href="#"><span>HOME</span></a>
            </li>
            <li>
                <a href="#"><span>RESERVATIONS</span></a>
            </li>
            <li>
                <a href="#"><span>MENU</span></a>
            </li>
            <li>
                <a href="#"><span>BLOG</span></a>
            </li>
            <li>
                <a href="#"><span>FEATURES</span></a>
            </li>
            <li>
                <a href="#"><span>CONTACT</span></a>
            </li>
        </ul>
    </nav>
    <div class="hero">
        <div class="arrow-left"></div>
        <div class="container">
            <div class="wrapper">
                <h2>The Best Cuisine<br><span>in your town</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <button>MENU</button>
            </div>
        </div>
        <div class="arrow-right"></div>
        <div class="pagination">
            <ul class="pagination-list">
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </ul>
        </div>
    </div>
</header>