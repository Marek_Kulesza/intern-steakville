<section>
<div class="hero">
    <div class="title">
        <h2><span class="wild">Our Staff</span><br>MEET OUR CHEFS</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div class="staff">
        <ul class="staff-list">
            <li>
                <img src="" alt="" class="square-img">
                <p>JAMES T KIRK<span class="wild"><br>Captain</span></p>
                <ul class="icons">
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore </p>
            </li>
            <li>
                <img src="" alt="" class="square-img">
                <p>LEONARD MCCOY<span class="wild"><br>Doctor Not A...</span></p>
                <ul class="icons">
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
            </li>
            <li>
                <img src="" alt="" class="square-img">
                <p>HIKARU SULU<span class="wild"><br>Pilot</span></p>
                <ul class="icons">
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                    <li><a href=""><span></span></a></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
            </li>
        </ul>
    </div>
</div>
</section>