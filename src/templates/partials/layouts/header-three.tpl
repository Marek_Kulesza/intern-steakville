<header class="l-header">
        <div class="top-bar-header-three">
            <div class="top-bar-header-three__icons">
                <ul class="top-bar__icons_list">
                    <li class="top-bar-header-three__item"><a href=""></a></li>
                    <li class="top-bar-header-three__item"><a href=""></a></li>
                    <li class="top-bar-header-three__item"><a href=""></a></li>
                </ul>
            </div>
        </div>
        <div class="l-navbar-header-three">
            <nav aria-label="main navigation" class="l-header__nav-header-three">
                <ul class="c-static-links-list-header-three">
                    <li>
                        <a href="#"><span>HOME</span></a>
                    </li>
                    <li>
                        <a href="#"><span>RESERVATIONS</span></a>
                    </li>
                    <li>
                        <a href="#"><span>MENU</span></a>
                    </li>
                    <li>
                        <a href="#"><span><img src="" alt="" class="logo-three"></span></a>
                    </li>
                    <li>
                        <a href="#"><span>BLOG</span></a>
                    </li>
                    <li>
                        <a href="#"><span>FEATURES</span></a>
                    </li>
                    <li>
                        <a href="#"><span>CONTACT</span></a>
                    </li>
                </ul>
            </nav>
            <aside class="aside-left"> <ul class="aside-icon-list">
                <li></li>
                <li></li>
                <li></li>
            </ul></aside>
            <div class="l-box-section-of">
                <div class="c-box-section-of">
                    <h2 class="c-box-section-of__title">ORGANIC FOOD</h2>
                    <figure><img src="" alt=""></figure>
                    <p class="c-box-section-of__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a href="" class="c-box-section-of__button">MENU</a>
                </div>
            </div>
            <div class="bottom-icon">
                <span class="down-scroll-icon"></span>
            </div>
        </div>
        </div>