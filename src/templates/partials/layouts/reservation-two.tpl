<section>
    <div class="l-hero">
        <div class="l-inner">
            <div class="c-title">
                <h2 class="c-title__head"><span class="font-wild"> </span><br></h2>
            </div>
            <div class="c-text">
                <p class="c-text__details"></p>
            </div>
            <div class="c-btn">
                <a href="" class="c-btn btn-gold"></a>
            </div>
        </div>
    </div>
</section>