<section>
    <div class="l-hero">
        <div class="l-inner">
            <div class="container">
                <div class="c-title">
                    <h2 class="c-title__head"><span class="font-wild"></span></h2>
                </div>
                <div class="img"><span class="arrow"></span></div>
                <div class="c-text">
                    <p class="text-details"></p>
                </div>
                <div class="c-text"></div>
                <p class="c-text__info">BAD COMEDIAN<br>
                    <span class="font-wild">Movie Critic</span></p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="l-hero">
        <div class="l-inner">
            <div class="subscription-box">
                <div class="c-title">
                    <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>
                </div>
                <div class="img">
                    <span class="arrow"></span>
                </div>
                <div class="input-btn">
                    <input type="email" name="" id="" value="Email">
                    <a href="" class="btn-gold">SUBSCRIBE</a>
                </div>
            </div>
        </div>
    </div>
</section>